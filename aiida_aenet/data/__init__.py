from aiida.orm import Data

# FIXME another plugin uses element-specific classes;
# model this class on the implementation

class AenetElement():
    """Element class for aenet potential environment elements"""
    def __init__(self, symbol: str, traits: dict, energy_units: str = "eV"):
        """Initialize class with passed trait dictionary"""

        self.symbol = symbol
        self.energy_units = energy_units
        self.set_traits(traits)

    def set_traits(self, trait_dict):
        """Set element instance attributes"""

        setattr(self, "energy", float(trait_dict['energy']))
        setattr(self, "nodes", int(trait_dict['nodes']))
        setattr(self, "network", list(trait_dict['network']))