from aiida.orm import load_node, List
from aiida.engine import submit

from aiida_aenet.calculations.generate import AenetGenerateCalculation
from aiida_aenet.data.algorithm import AenetAlgorithm

elements = {
    "Ni": {
        "energy":
        -4676.3936784796315,
        "nodes":
        2,
        "network": [{
            "nodes": 2,
            "activation": "tanh"
        }, {
            "nodes": 2,
            "activation": "tanh"
        }]
    },
    "P": {
        "energy":
        -194.14828627169916,
        "nodes":
        2,
        "network": [{
            "nodes": 2,
            "activation": "tanh"
        }, {
            "nodes": 2,
            "activation": "tanh"
        }]
    }
}

chebyshev = {
    "type": "chebyshev",
    "parameters": {
        "radial_rc": 4.0,
        "radial_n": 6,
        "angular_rc": 4.0,
        "angular_n": 2
    }
}

parameters = {
    "test_percent": 10,
    "epochs": 10,
    "max_energy": 0.0,
    "r_min": 0.75
}

nn_algorithm = AenetAlgorithm(
    elements=elements,
    descriptor=chebyshev,
    training="bfgs",
    parameters=parameters,
)

nn_algorithm.label = "example AenetAlgorithm instance"
reference_list = [10753, 10754]

inputs = {
    "code": load_node(10738),
    "reference": List(list=reference_list),
    "algorithm": nn_algorithm
}

calc_node = submit(AenetGenerateCalculation, **inputs)