from aiida_lammps.data.potential import EmpiricalPotential
from aiida.orm import load_node

# TODO CLEAN SCRIPT

chebyshev = {
    "type": "chebyshev",
    "parameters": {
        "radial_rc": 4.0,
        "radial_n": 6,
        "angular_rc": 4.0,
        "angular_n": 2
    }
}

bp = {
    "type": "behler",
    "parameters": {
        "cutoff": None,
        "G2": {
            "etas": []
        },
        "G4": {
            "etas": [],
            "lambdas": [],
            "zetas": []
        }
    }
}

# TODO implement default node creation and calculation parameters

def get_pure_Ni_structure():
    return

def get_pure_P_structure():
    return

def get_alloy_NiP_structure():
    """Returns Ni3P structure (n=96)"""
    
    return load_node(5500)

def get_NiP_eam_potential():

    eam = EmpiricalPotential({
        'type': 'eam',
        'file_contents': open('/Users/nicholas/gitwork/bmg/NiP/NiP.lammps.eam').readlines()
    })

    return eam

from aiida_quantumespresso import __init__
from aiida_lammps.tests.utils import lammps_version

t0 = 10000
tf = 10000000
dt = 10000
times = [str(t) for t in range(t0, tf+1, dt)]
times_str = ' '.join(times)

md = {
    'equilibration': {
        'code': 1,
        'structure_data': '430.init.dat',
        'temperature': 300,
        # 'min_style': 'sd',
        # 'min_params': [1.0e-4, 1.0e-6, 100],
        # 'timestep': 0.001,
        # 'n_steps': 1000
    },
    'heating': {
        'code': 2,
        'structure_data': '430.hot.dat',
        'temperature': 1000,
        'timestep': 0.001,
        'dump_rate': 10000,
        'n_steps': 10000000
    },
    'quenching': {
        'code': 3,
        'temperature': 300,
        't0': t0,
        'tf': tf,
        'dt': dt,
        'times': times_str,
        'loops': len(times),
        # 'min_style': 'sd',
        # 'min_params': [1.0e-4, 1.0e-6, 100],
        # 'n_steps': 1000
    }
}

aiida_lmp = {
    'heating': {
        'lammps_version': lammps_version(),
        'units': 'metal',
        'timestep': 0.001,
        'integration': {
            'style': 'npt',
            'constraints': {
                'temp': [1000, 1000, 0.1],
                'iso': [0., 0., 1.0]
            },
        'total_steps': 1000
        },
        'total_steps': 1000,
        'dump_rate': 10,
        'restart': 100
    },
    'quenching': {
        'lammps_version': lammps_version(),
        'units': 'metal',
        'relax': {
            'type': 'iso',
            'pressure': 0.0,
            'vmax': 0.001,
        },
        "minimize": {
            'style': 'sd',
            'energy_tolerance': 1.0e-4,
            'force_tolerance': 1.0e-6,
            'max_evaluations': 100,
            'max_iterations': 1000
        }
    }
}

pw = {
    "CONTROL" : {
        "calculation" : "scf",
        "restart_mode" : "from_scratch",
        "tprnfor" : True,
        "verbosity" : "low"
    },
    "SYSTEM" : {
        "occupations" : "smearing",
        "smearing" : "gaussian",
        "degauss" : 0.35
    },
    "ELECTRONS" : {
        "conv_thr" : 5e-6,
        "diago_david_ndim" : 4,
        "mixing_mode" : "local-TF",
        "mixing_beta" : 0.6
    }
}

CUR = {
    'type': 'BP', # TODO Descriptor class attr
    'r_cut': 8.0, # TODO Descriptor class attr
    'G': 2,
    'cutoff_fun': 'cos',
    'eta': [
        'internal',
        13,
        1.670721609,
        52.42546894
    ],
    'json_file': 'reference.json'
}

aenet = {
    'key': -1
}