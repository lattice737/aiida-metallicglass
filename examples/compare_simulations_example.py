from aiida.orm import load_node, Dict
from aiida.engine import submit

from aiida_aenet.workflows.compare_simulations import CompareSimulationsWorkChain
from aiida_aenet.data.potentials import AenetPotential

from aiida_lammps.data.potential import EmpiricalPotential

elements = ["Ni", "P"]
ann_files = ["/Users/nicholas/Ni.nn", "/Users/nicholas/P.nn"]

content_dict = {
    f"{X}.nn": open(file, 'rb').read()
    for X, file in zip(elements, ann_files)
}
ann_data = {'file_contents': content_dict}

eam_file = '/Users/nicholas/gitwork/bmg/NiP/NiP.lammps.eam'
eam_data = {'type': 'alloy', 'file_contents': open(eam_file).readlines()}

stagelist = [{
    'name': 'nvt',
    'steps': 100_000,
    'integration': {
        'style': 'nvt',
        'constraints': {
            'temp': [300, 300, 0.1]
        },
    },
    'output_atom': {
        'dump_rate': 1000
    }
}]
simulation_inputs = {
    'parameters':
    Dict(dict={
        'units': 'metal',
        'timestep': 0.001,
        'stages': stagelist
    }),
    'metadata': {
        'label': "empirical v. ann",
        'description': 'nvt with aiida-lammps using aenet-lammps build',
        'options': {
            'withmpi': True,
            'resources': {
                'num_machines': 1,
                'num_mpiprocs_per_machine': 4
            }
        }
    },
}

inputs = Dict(
    dict={
        "code": load_node(11018),
        "structure": load_node(98),
        "empirical_potential": EmpiricalPotential(type="eam", data=eam_data),
        "ann_potential": AenetPotential(data=ann_data),
        "simulation": simulation_inputs,
    })

calc_node = submit(CompareSimulationsWorkChain, **inputs)