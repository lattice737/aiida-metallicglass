from aiida.orm import load_node, Dict
from aiida.engine import submit

from aiida_aenet.calculations.simulate import AenetLammpsMdCalculation
from aiida_aenet.data.potentials import AenetPotential

path = "/Users/nicholas/gitwork/bmg/ann/bulk/435+5"

elements = ["Ni", "P"]
ann_files = [f"{path}/Ni.nn", f"{path}/P.nn"]

content_dict = {
    f"{X}.nn": open(file, 'rb').read()
    for X, file in zip(elements, ann_files)
}
ann_data = {'file_contents': content_dict}

Tmax = 1500
Tmin = 300

stagelist = [{
    'name': 'npt',
    'steps': 1_200_000,
    'integration': {
        'style': 'npt',
        'constraints': {
            'temp': [Tmax, Tmin, 0.1],
            'iso': [0, 0, 0.5]
        },
    },
    'output_atom': {
        'dump_rate': 1200
    }
}]
metadata = {
    'label': "aenet-lammps",
    'description': 'npt with aiida-lammps using aenet-lammps build',
    'options': {
        'withmpi': True,
        'resources': {
            'num_machines': 1,
            'num_mpiprocs_per_machine': 4
        }
    }
}
inputs = Dict(
    dict={
        "code":
        load_node(1718),
        "structure":
        load_node(909),
        "potential":
        AenetPotential(data=ann_data),
        'parameters':
        Dict(dict={
            'units': 'metal',
            'timestep': 0.002,
            'stages': stagelist
        }),
        'metadata':
        metadata,
    })

calc_node = submit(AenetLammpsMdCalculation, **inputs)