import pytest

from aiida.plugins import DataFactory

pytest_plugins = ["aiida.manage.tests.pytest_fixtures"]


@pytest.fixture(scope="function")
def get_structure_data():
    """StructureData for testing."""

    # cell
    # symbols
    # positions

    structure = DataFactory("structure")(cell=cell)

    for element, position in zip(symbols, positions):

        structure.append_atom(position=tuple(position), kind_name=element)

    return potential_instance


@pytest.fixture(scope="function")
def get_potential_data():
    """AenetPotential for testing."""

    from aiida_aenet.data.algorithm import AenetPotential

    # TODO sample instantiation

    potential_instance = AenetPotential()  # FIXME need to pass arguments

    return potential_instance


@pytest.fixture(scope="function")
def get_aenet_data():
    """aenet data for testing."""

    #aenet_dict = {
    #    "structure": int,
    #    "pw": int
    #}

    return


#@pytest.fixture(scope="function")
#def diff_code(aiida_local_code_factory):
#    """Get a diff code."""
#    return aiida_local_code_factory(executable="diff", entry_point="diff")


@pytest.fixture(scope="function", autouse=True)
def clear_database_auto(clear_database):
    """Automatically clear database in between tests."""
    pass